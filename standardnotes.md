```
sudo yum -y install git
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

```
cd ~
git clone --single-branch --branch main https://github.com/standardnotes/standalone.git
cd standalone
```

```
sudo ./server.sh setup
```

```
openssl rand -hex 32
```

```
$ ./server.sh start
```
