# Rocky Linux 8 Mediaserver

`Rocky-8.5-x86_64-boot.iso` 

SHA256: `5a0dc65d1308e47b51a49e23f1030b5ee0f0ece3702483a8a6554382e893333c`

## Installation

### Localization

en-US; Europe/Berlin

### System

**Network & Host Name:** Ethernet (ens3) `ON`; Hostname: `<example>` ; Wait!; `Done`

**Installation Destination:**

- [x] I would like to make additional space available.

`Done`; `Delete all`; `Reclaim Space`

### Software

**Software Selection:** Server

### User Settings

**User Creation:** 

Full & User Name: `<example>`

- [x] Make this User administrator

### Installation Summary

![](/img/installation-summary.png)

`Begin Installation`

### Cockpit

```
systemctl enable --now cockpit.socket
```

https://12.34.56.78:9090/

**Change Login Background:** 
```
sudo mv <example>.jpg /usr/share/cockpit/branding/default/bg-plain.jpg
```
`Enter`

### Config

Services > mcelog `OFF`

Applications > Podman `Remove`

SELinux > SELinux policy `OFF`

### Docker

```
sudo yum install -y yum-utils
```

```
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

```
sudo yum -y install docker-ce docker-ce-cli containerd.io --allowerasing
```

```
sudo systemctl start docker
```

```
 sudo systemctl enable docker.service
```

```
 sudo systemctl enable containerd.service
```

### Portainer

```
sudo docker volume create portainer_data
```

```
sudo docker run -d -p 8000:8000 -p 9443:9443 --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:2.11.1
```

https://12.34.56.78:9443/

**Environment Wizard:** `Get Started (Proceed using the local environment which Portainer is running in)`


#### Fixing 0.0.0.0

1. **Enviroments** > local > Public IP: `<example>`

#### New Stack

1. **Stacks** > `Add Stack`
2. `git Repository`
3. Repository URL: `https://gitlab.com/<example>/<example>.git`; Repository reference: `refs/heads/main`

#### Updating Stacks

1. **Stacks** > Stack details > `Stop this stack`
2. **Images** > Mark images > `Force Remove`
3. **Stacks** > Stack details > `Pull and redeploy`


### SABnzbd

**SFTP:** `[...]/_config/sabnzbd/sabnzbd.ini` > `inet_exposure = 4`

https://12.34.56.78:6767/

SSL `ON`


### nginx Proxy Manager

Everything (except "Use a DNS Challenge") `ON`

### Nextcloud

nginx Proxy Manager > Edit Proxy Host > Advanced:

```
rewrite ^/\.well-known/carddav https://cloud.2ym.de/remote.php/dav/ redirect;
rewrite ^/\.well-known/caldav https://cloud.2ym.de/remote.php/dav/ redirect;
```

Edit `/var/lib/docker/volumes/cloud_nextcloud/_data/config/config.php`:

(At the end)

```
#  'installed' => true,
  'default_phone_region' => 'DE',
  'overwriteprotocol' => 'https',
  'skeletondirectory' => '',
#);
```

Delete Everything in `/var/lib/docker/volumes/cloud_nextcloud/_data/core/skeleton`

### rclone

```
curl https://rclone.org/install.sh | sudo bash
```

**sudo!!!**

#### Configure remote
```
sudo rclone config
```
Google Drive Root Folder ID: (example: 0ACX9fDyuop0HUk9PVA)

https://drive.google.com/drive/folders/_root-folder-id_/


#### HTTP File Server
```
sudo rclone serve http <remote-name>:<path> --addr :<port>
```

**=> nginx Proxy Manager !!!**

#### Mount

```
sudo rclone mount <remote-name>:<path> </localpath>
```

### FFMPEG

```
sudo dnf install rpmfusion-free-release
dnf config-manager --set-enabled powertools
sudo dnf install ffmpeg
```
Variante 1:
`ffmpeg -i input.mkv -b:v 1000K -b:a 64k -speed 4 output.webm`
